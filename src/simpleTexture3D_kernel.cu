/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#ifndef _SIMPLETEXTURE3D_KERNEL_CU_
#define _SIMPLETEXTURE3D_KERNEL_CU_


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <helper_cuda.h>
#include <helper_math.h>

typedef unsigned int  uint;
typedef unsigned char uchar;
typedef uchar4 typeImg;

texture<typeImg, 3, cudaReadModeElementType> tex;  // 3D texture

cudaArray *d_volumeArray = 0;

__device__ int rgbToInt(float r, float g, float b, float w)
{
    r = clamp(r, 0.0f, 255.0f);
    g = clamp(g, 0.0f, 255.0f);
    b = clamp(b, 0.0f, 255.0f);
    w = clamp(w, 0.0f, 255.0f);
    return (int(w)<<24) | (int(b)<<16) | (int(g)<<8) | int(r);
}

__global__ void
d_render(uint *d_output, uint imageW, uint imageH, float w)
{
    uint x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
    uint y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;

    // read from 3D texture
    uchar4 voxel = tex3D(tex, x, y, w);

    if ((x < imageW) && (y < imageH))
    {
        // write output color
        uint i = __umul24(y, imageW) + x;
        d_output[i] = rgbToInt(voxel.x,voxel.y,voxel.z,voxel.w);
    }
}

extern "C"
void initCuda(const typeImg *h_volume, cudaExtent volumeSize)
{
	void *dBuf;
	cudaMalloc((void**)&dBuf, 120*sizeof(uchar4) * volumeSize.width * volumeSize.height);
	cudaMemcpy(dBuf, h_volume, 120*sizeof(uchar4) * volumeSize.width * volumeSize.height, cudaMemcpyHostToDevice);

    // create 3D array
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar4>();
    checkCudaErrors(cudaMalloc3DArray(&d_volumeArray, &channelDesc, volumeSize));

    // copy data to 3D array
    cudaMemcpy3DParms copyParams = {0};
    copyParams.srcPtr   = make_cudaPitchedPtr((void *)dBuf, volumeSize.width*sizeof(uchar4), volumeSize.width, volumeSize.height);
    copyParams.dstArray = d_volumeArray;
    copyParams.extent   = volumeSize;
    copyParams.kind     = cudaMemcpyDeviceToDevice;
    checkCudaErrors(cudaMemcpy3D(&copyParams));

    // set texture parameters
    tex.normalized = false;                      // access with normalized texture coordinates
//    tex.filterMode = cudaFilterModeLinear;      // linear interpolation
    tex.addressMode[0] = cudaAddressModeWrap;   // wrap texture coordinates
    tex.addressMode[1] = cudaAddressModeWrap;
    tex.addressMode[2] = cudaAddressModeWrap;

    // bind array to 3D texture
    checkCudaErrors(cudaBindTextureToArray(tex, d_volumeArray, channelDesc));
}

extern "C"
void render_kernel(dim3 gridSize, dim3 blockSize, uint *d_output, uint imageW, uint imageH, float w)
{
    d_render<<<gridSize, blockSize>>>(d_output, imageW, imageH, w);
}

#endif // #ifndef _SIMPLETEXTURE3D_KERNEL_CU_
