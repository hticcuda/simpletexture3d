################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CU_SRCS += \
../src/simpleTexture3D_kernel.cu 

CPP_SRCS += \
../src/simpleTexture3D.cpp 

OBJS += \
./src/simpleTexture3D.o \
./src/simpleTexture3D_kernel.o 

CU_DEPS += \
./src/simpleTexture3D_kernel.d 

CPP_DEPS += \
./src/simpleTexture3D.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-8.0/bin/nvcc -I"/usr/local/cuda-8.0/samples/2_Graphics" -I"/usr/local/cuda-8.0/samples/common/inc" -I"/home/htic/Desktop/GPU/simpleTexture3D" -G -g -O0 -gencode arch=compute_20,code=sm_20 -m64 -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-8.0/bin/nvcc -I"/usr/local/cuda-8.0/samples/2_Graphics" -I"/usr/local/cuda-8.0/samples/common/inc" -I"/home/htic/Desktop/GPU/simpleTexture3D" -G -g -O0 --compile -m64  -x c++ -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-8.0/bin/nvcc -I"/usr/local/cuda-8.0/samples/2_Graphics" -I"/usr/local/cuda-8.0/samples/common/inc" -I"/home/htic/Desktop/GPU/simpleTexture3D" -G -g -O0 -gencode arch=compute_20,code=sm_20 -m64 -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-8.0/bin/nvcc -I"/usr/local/cuda-8.0/samples/2_Graphics" -I"/usr/local/cuda-8.0/samples/common/inc" -I"/home/htic/Desktop/GPU/simpleTexture3D" -G -g -O0 --compile --relocatable-device-code=false -gencode arch=compute_20,code=compute_20 -gencode arch=compute_20,code=sm_20 -m64  -x cu -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


